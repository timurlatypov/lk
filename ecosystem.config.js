module.exports = {
    apps: [
        {
            name: "lk2",
            script: "./bin/www",
            env: {
                PORT: 3000,
                NODE_ENV: "production"
            }
        }
    ],
    deploy: {
        production: {
            user: "ubuntu",
            host: "18.184.6.153",
            key: "~/.ssh/test.pem",
            ref: "origin/master",
            repo: "https://timurlatypov@bitbucket.org/timurlatypov/lk2.git",
            path: "/home/ubuntu/lk2",
            "post-deploy":
                "npm install && pm2 startOrRestart ecosystem.config.js"
        }
    }
};