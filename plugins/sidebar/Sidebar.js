const Sidebar = {
    install (Vue) {
        this.event = new Vue()

        Vue.prototype.$sidebar = {
            show (sidebar, params = {}) {
                Sidebar.event.$emit('show', sidebar, params)
            },
            hide (sidebar) {
                Sidebar.event.$emit('hide', sidebar)
            },
            $event: this.event
        }
    }
}

export default Sidebar