const dictionary = {
    ru: {
        attributes: {
            email: 'Email',
            first_name: 'Имя',
            last_name: 'Фамилия',
            old_password: 'Старый пароль',
            password: 'Пароль',
            password_confirm: 'Подтвердить пароль',
            phone_number: 'Телефон',
            sms_code: 'SMS-Код',

            klw_code: 'KLW-код',
            voucher: 'KLW-код',
            totp: 'Одноразовый код',

            get_old_password: '<b>Действующий пароль</b>',
            get_new_password: '<b>Новый пароль</b>',
            get_confirm_new_password: '<b>Подтвердить новый пароль</b>',
            agreement: '<b>Соглашение</b>',

            agreement_1: 'Согласен',
            agreement_2: 'Согласен',
            agreement_3: 'Согласен',
            agreement_4: 'Согласен',
            agreement_5: 'Согласен',

            tokens: '<b>Токен</b>',
            tokens_amount: '<b>Количество токенов</b>',
            wallet: '<b>Адрес кошелька</b>',
            token_name: '<b>Токен для покупки</b>',
            currency: '<b>Валюта оплаты</b>',
        }
    }
};
export default dictionary