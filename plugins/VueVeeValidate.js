import Vue from 'vue'

import ru from 'vee-validate/dist/locale/ru';
import VeeValidate, { Validator } from 'vee-validate';

// Install the Plugin.
Vue.use(VeeValidate, {
    events: 'change'
});

// Localize takes the locale object as the second argument (optional) and merges it.
import dictionary from './vee-validate/dictionary.js'

Validator.localize(dictionary);
Validator.localize('ru', ru);
