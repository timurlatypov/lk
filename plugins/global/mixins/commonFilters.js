import Vue from 'vue'
import numeral from 'numeral'

Vue.mixin({
    filters: {
        twoDecimal: function (val) {
            return val.toFixed(2)
        },
        eightDecimal: function (val) {
            return val.toFixed(8)
        }
    }
})