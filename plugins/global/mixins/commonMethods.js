import Vue from 'vue'
import numeral from 'numeral'

Vue.mixin({
    methods: {
        copyAddress(id) {
            let copyText = document.getElementById(id);
            copyText.select();
            document.execCommand("copy");
        },
        invested (project) {
            return numeral(project).format('$0,0');
        },
        dollars_decimals (project) {
            return numeral(project).format('$0,0.00');
        },
        decimals (project) {
            return numeral(project).format('0,0.00');
        },
        defineColorClass(bonusType) {
            if (bonusType === 'Credit') {
                return 'u-color-success'
            } else if (bonusType === 'debit') {
                return 'u-color-danger'
            } else if (bonusType === 'TokenPayment') {
                return 'u-color-danger'
            } else if (bonusType === 'TokenPurchase') {
                return ''
            } else {
                return 'u-color-success'
            }
        },
        defineTranslation(bonusType) {
            if (bonusType === 'Credit') {
                return 'Пополнение баланса'
            } else if (bonusType === 'debit') {
                return 'Сумма списания'
            } else if (bonusType === 'TokenPayment') {
                return 'Оплата токенов'
            } else if (bonusType === 'TokenPurchase') {
                return 'Начисление токенов'
            } else if (bonusType === 'Bonus') {
                return 'Бонус за регистрацию'
            } else if (bonusType === 'CommissionBonus') {
                return 'Комиссионное начисление'
            } else if (bonusType === 'FirstAuthInApp') {
                return 'Бонус за регистрацию'
            } else if (bonusType === 'FriendRegistrationBonus') {
                return 'Регистрация друга'
            } else {
                return
            }
        },
        currency(id) {
            if (id === 'usd') {
                return 'USDT';
            } else if (id === 'rub') {
                return 'RUB';
            } else if (id === 'btc') {
                return 'BTC';
            } else if (id === 'eth') {
                return 'ETH';
            } else {
                return id;
            }
        },
    }
})