const pkg = require('./package')

module.exports = {
    mode: 'universal',

    /*
    ** Headers of the page
    */
    head: {
        title: 'Klooway Technologies Investor Dashboard',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: '' }
        ],
        script: [
            { src: '/js/neat.js' }
        ],
        link: [
            { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Lato' },
            { rel: 'stylesheet', href: 'https://use.fontawesome.com/releases/v5.5.0/css/all.css', integrity: 'sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU', crossorigin: 'anonymous' },
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
        ]
    },

    /*
    ** Customize the progress-bar color
    */
    loading: { color: '#2083FE' },

    /*
    ** Global CSS
    */
    css: [
        '~assets/styles/app.scss'
    ],
    /*
    ** Plugins to load before mounting the App
    */
    plugins: [
        '@plugins/VueModal',
        '@plugins/VueSidebar',
        '@plugins/VueMoment',
        '@plugins/VueLodash',
        '@plugins/VueCookie',
        '@plugins/VueVeeValidate',
        '@plugins/global/mixins/commonMethods',
        '@plugins/global/mixins/commonFilters',
    ],

    auth: {
        strategies: {
            local: {
                endpoints: {
                    login: {
                        url: 'auth/login',
                        method: 'POST',
                        propertyName: 'meta.token'
                    },
                    logout: {
                        url: 'auth/logout',
                        method: 'GET',
                    },
                    user: {
                        url: 'auth/user',
                        method: 'get',
                        propertyName: 'user'
                    }
                },
                rewriteRedirects: true,
                resetOnError: true,
                redirect: {
                    login: '/login',
                    logout: '/login',
                    home: '/'
                },
            }
        }
    },
    /*
    ** Nuxt.js modules
    */
    modules: [
        // Doc: https://github.com/nuxt-community/axios-module#usage
        '@nuxtjs/axios',
        '@nuxtjs/auth',
        'numeral'
    ],
    /*
    ** Axios module configuration
    */
    axios: {
        baseURL: 'https://staging.klooway.com/api'
        //baseURL: 'http://localhost:8080/api'
    },

    /*
    ** Build configuration
    */
    build: {
        /*
        ** You can extend webpack config here
        */
        extend(config, ctx) {

        }
    }
}
